import Express              from 'express';
import del                  from 'del';
import webpack              from 'webpack';
import fs                   from 'fs';
import WriteFilePlugin      from 'write-file-webpack-plugin';
import webpackDevMiddleware from "webpack-dev-middleware";
import webpackHotMiddleware from "webpack-hot-middleware";

import webpackConfig from './webpack.config.babel';

cleanDist();

webpackConfig.plugins = webpackConfig.plugins.concat([
  new WriteFilePlugin()
]);
webpackConfig.entry = webpackConfig.entry.concat([
  'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
]);

const compiler = webpack(webpackConfig);
const app      = new Express();

app.use(webpackDevMiddleware(compiler, {
  publicPath:   webpackConfig.output.publicPath,
  quiet:        false,
  watchOptions: {
    aggregateTimeout: 300,
    poll:             1000
  },
  historyApiFallback: true,
  cache:              true,
  colors:             true,
  host:               '127.0.0.1',
  stats: {
    colors:       true,
    errors:       true,
    warnings:     true,
    chunks:       false,
    version:      false,
    assets:       false,
    timings:      false,
    entrypoints:  false,
    chunkModules: false,
    chunkOrigins: false,
    cached:       false,
    cachedAssets: false,
    reasons:      false,
    usedExports:  false,
    children:     false,
    source:       false,
    modules:      false
  }
}));

app.use(webpackHotMiddleware(compiler, {
  log:       console.log,
  path:      '/__webpack_hmr',
  heartbeat: 10 * 1000,
}));

app.get('*', (req, res) => {
  const index = fs.readFileSync('./dist/index.html');
  res.end(index);
});

app.listen(8000, 'localhost', err => {
  if (err) {
    return console.log(err);
  }

  console.log('Listening at http://localhost:8000/');
});

function cleanDist() {
    del.sync('./dist', {force: true});
}