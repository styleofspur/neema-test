import webpack           from 'webpack';
import path              from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import bootstrapStylus   from 'bootstrap-styl';

const paths = {
    src:    path.resolve(__dirname, './src'),
    vendor: path.resolve(__dirname, './node_modules'),
    dist:   path.resolve(__dirname, './dist')
};

const baseConfig = {
    cache:   true,
    watch:   true,
};

const loaders = {
    babel: {
        test:    /\.js$/,
        loaders: ['babel'],
        include: paths.src
    },
    stylus: {
        test:    /\.styl$/,
        loader:  'style!css!stylus?paths=node_modules/bootstrap-styl'
    },
    fonts: [
        {
            test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=10000&mimetype=application/font-woff&prefix=fonts&name=font.[name].[ext]'
        }, {
            test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=10000&mimetype=application/octet-stream&prefix=fonts&name=font.[name].[ext]'
        }, {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=10000&mimetype=application/vnd.ms-fontobject&prefix=fonts&name=font.[name].[ext]'
        }, {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=10000&mimetype=image/svg+xml&prefix=fonts&name=font.[name].[ext]'
        }
    ]
};

const plugins = {
    commonChunks: () => {
        return new webpack.optimize.CommonsChunkPlugin({
            name:      'vendors',
            filename:  'vendors.[hash].js',
            minChunks: (module, count) => {
                return module.resource && module.resource.indexOf('node_modules') !== -1 && count >= 1;
            }
        });
    },
    html: (options) => {
        return new HtmlWebpackPlugin(options);
    },
    provide: () => {
        return new webpack.ProvidePlugin({
            React: 'react'
        });
    },
    sourceMap: (path) => {
        return new webpack.SourceMapDevToolPlugin({
            filename: '[file].map',
            append:   `\n//# sourceMappingURL=${path}[url]`
        })
    },
    hmr: () => {
        return new webpack.HotModuleReplacementPlugin();
    }
};

// resolves
const resolve = {
    modulesDirectories: ['node_modules'],
};

// config data per entry
const entryPoints = {
    context: paths.src,
    entry: [
        './app'
    ],
    resolve: resolve,
    module:  {
        loaders: [
            loaders.babel,
            loaders.stylus,
            loaders.fonts,
        ]
    },
    output:  {
        path:          paths.dist,
        publicPath:    '/',
        filename:      'app.[chunkhash].js',
        chunkFilename: '[name].[chunkhash].js'
    },
    plugins: [
        plugins.commonChunks(),
        plugins.provide(),
        plugins.sourceMap('/'),
        plugins.html({
            template: path.join(paths.src, 'index.html'),
            inject:   'body',
            filename: 'index.html'
        }),
        plugins.hmr()
    ]
};

export default Object.assign({}, entryPoints, baseConfig);
