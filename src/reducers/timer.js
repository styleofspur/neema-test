import { ActionsEnum } from '../utils/enums';

const initialState = {
    seconds:   0,
    isRunning: false
};

export default (state = initialState, action) => {

    switch (action.type) {
        case ActionsEnum.TIMER.START:
            return Object.assign({}, state, {isRunning: true});
        case ActionsEnum.TIMER.INCREASE_TIME:
            return Object.assign({}, state, {seconds: state.seconds + 1});
        case ActionsEnum.TIMER.PAUSE:
            return Object.assign({}, state, {isRunning: false});
        case ActionsEnum.TIMER.CLEAR:
            return Object.assign({}, state, {isRunning: false, seconds: 0});
        default:
            return state;
    }
};