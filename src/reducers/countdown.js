import { ActionsEnum } from '../utils/enums';

const initialState = {
    seconds:   0,
    isRunning: false
};

export default (state = initialState, action) => {

    switch (action.type) {
        case ActionsEnum.COUNTDOWN.START:
            return Object.assign({}, state, {isRunning: true, seconds: action.seconds});
        case ActionsEnum.COUNTDOWN.DECREASE_TIME:
            return Object.assign({}, state, {seconds: state.seconds - 1});
        case ActionsEnum.COUNTDOWN.PAUSE:
            return Object.assign({}, state, {isRunning: false});
        case ActionsEnum.COUNTDOWN.CLEAR:
            return Object.assign({}, state, {isRunning: false, seconds: 0});
        default:
            return state;
    }
};