import Timer             from './pages/timer/timer.component';
import { App }           from './containers';
import { AppRoutesEnum } from './utils/enums';

export default {
        component: App,
        path:      AppRoutesEnum.TIMER,
        indexRoute: {
            component: Timer
        },
        childRoutes: getRoutesPaths()
};

function getRoutesPaths() {
    let initialized = [];

    [
        'timer',
        'countdown'
    ].forEach(pageName => {
        initialized.push(require(`./pages/${pageName}/${pageName}.route`));
    });

    return initialized;
}