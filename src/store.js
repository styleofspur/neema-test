import { createStore, combineReducers, compose } from 'redux';
import * as reducers                             from './reducers/index';

let reducer = combineReducers(reducers);
let lastCreatedStore; // <------------------------------- remember store
const hmr = () => {
    if (module.hot) {
        module.hot.accept('./reducers', () => {
            reducer = combineReducers(require('./reducers/index'));
            lastCreatedStore.replaceReducer(reducer);
        });
    }
};

hmr();

export default function() {
    let finalCreateStore = compose(
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )(createStore);
    
    const store = finalCreateStore(reducer, {});
    lastCreatedStore = store; // <------------------ remember store

    return store;
}