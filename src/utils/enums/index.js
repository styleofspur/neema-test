import Actions   from './actionsEnum';
import AppRoutes from './appRoutesEnum';

export const ActionsEnum    = Actions;
export const AppRoutesEnum = AppRoutes;

if (module.hot) {
    module.hot.accept();
}