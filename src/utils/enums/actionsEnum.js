/**
 * Represents a list of application actions
 */
export default class ActionsEnum {

    static TIMER = {
        START:         'START_TIMER',
        INCREASE_TIME: 'INCREASE_TIMER_TIME',
        PAUSE:         'PAUSE_TIMER',
        CLEAR:         'CLEAR_TIMER'
    };

    static COUNTDOWN = {
        START:         'START_COUNTDOWN',
        DECREASE_TIME: 'DECREASE_COUNTDOWN_TIME',
        PAUSE:         'PAUSE_COUNTDOWN',
        CLEAR:         'CLEAR_COUNTDOWN'
    };
}