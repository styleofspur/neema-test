import TimeComponent          from './time/time.component';
import ButtonsHolderComponent from './buttonsHolder/buttonsHolder.component';

export const Time          = TimeComponent;
export const ButtonsHolder = ButtonsHolderComponent;

if (module.hot) {
    module.hot.accept();
}