import './time.styl';

export default class Time extends React.Component {

    static elementClass = 'time';

    static propTypes = {
        cssClass: React.PropTypes.string,
        seconds:  React.PropTypes.number
    };

    static defaultProps = {
        cssClass: '',
        seconds:  0
    };

    constructor() {
        super(...arguments);

        this.getTime = this.getTime.bind(this);
    }

    /**
     *
     * @returns {XML}
     */
    getTime() {
        let seconds = this.props.seconds % 60;
        let minutes = Math.floor(this.props.seconds / 60);

        if (minutes === 0) {
            minutes = '00';
        } else {
            minutes = minutes < 10 ? `0${minutes}` : minutes;
        }

        if (seconds === 0) {
            seconds = '00';
        } else {
            seconds = seconds < 10 ? `0${seconds}` : seconds;
        }

        return <span>{minutes}:{seconds}</span>;
    }

    /**
     *
     * @returns {XML}
     */
    render() {

        return (
            <div className={`${Time.elementClass} text-center ${this.props.cssClass}`}>
                <div className="numbers-holder">
                    <p>
                        {this.getTime()}
                    </p>
                </div>
            </div>
        );
    }
}