import './buttonsHolder.styl';

export default class ButtonsHolder extends React.Component {

    static elementClass = 'buttons-holder';

    static propTypes = {
        isRunning: React.PropTypes.bool,
        onStart:   React.PropTypes.func.isRequired,
        onPause:   React.PropTypes.func.isRequired,
        onClear:   React.PropTypes.func.isRequired,
    };

    static defaultProps = {
        isRunning: false
    };

    render() {

        return (
            <div className={`${ButtonsHolder.elementClass} center-block`}>
                {
                    this.props.isRunning ?
                        <button className="btn btn-pause" onClick={this.props.onPause}>Pause</button> :
                        <button className="btn btn-start" onClick={this.props.onStart}>Start</button>
                }
                <button className="btn btn-clear" onClick={this.props.onClear}>Clear</button>
            </div>
        );
    }
}