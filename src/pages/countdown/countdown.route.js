import { AppRoutesEnum } from '../../utils/enums';

export default {
    path: AppRoutesEnum.COUNTDOWN,
    getComponent(location, cb) {
        require.ensure([], (require) => {
            cb(null, require('./countdown.component'))
        }, 'countdown');
    }
};

if (module.hot) {
    module.hot.accept();
}