import { connect } from 'react-redux';

import { CountdownActions } from '../../actions'
import {
    Time,
    ButtonsHolder
}  from '../../components';
import './countdown.styl';

class Countdown extends React.Component {

    _countdown = null;

    static elementClass = 'countdown';

    constructor() {
        super(...arguments);

        this.startCountdown = this.startCountdown.bind(this);
        this.pauseCountdown = this.pauseCountdown.bind(this);
        this.clearCountdown = this.clearCountdown.bind(this);
    }

    /**
     *
     * @param evt
     */
    startCountdown(evt) {
        evt.preventDefault();

        const seconds = this.props.seconds || +document.querySelector('#secondsInput').value;
        this.props.dispatch(CountdownActions.start(seconds));

        this._countdown = setInterval(() => {
            this.props.seconds ?
                this.props.dispatch(CountdownActions.decreaseTime()) : this.clearCountdown();
        }, 1000);
    }

    pauseCountdown() {
        clearInterval(this._countdown);
        this._countdown = null;
        this.props.dispatch(CountdownActions.pause());
    }

    clearCountdown() {
        if (this._countdown) {
            clearInterval(this._countdown);
            this._countdown = null;
        }

        this.props.dispatch(CountdownActions.clear());
    }

    componentWillUnmount() {
        this.clearCountdown();
    }
    
    render() {
        return (
            <div className={Countdown.elementClass}>
                <h1 className="text-center">Countdown App</h1>
                <div className="time-holder center-block">
                    <Time cssClass="center-block" seconds={this.props.seconds}/>
                    {
                        this.props.seconds ?
                            <ButtonsHolder
                                isRunning={this.props.isRunning}
                                onStart={this.startCountdown}
                                onPause={this.pauseCountdown}
                                onClear={this.clearCountdown}
                            /> :
                            <form className="seconds-input-holder center-block" onSubmit={this.startCountdown}>
                                <input
                                    className="form-control"
                                    type="number"
                                    defaultValue={1}
                                    min={1}
                                    id="secondsInput"
                                />
                                <button type="submit" className="btn btn-block btn-start">Start</button>
                            </form>
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({countdown}) => {
    return {...countdown};
};

export default connect(mapStateToProps)(Countdown);