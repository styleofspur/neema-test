import { AppRoutesEnum } from '../../utils/enums';

export default {
    path: AppRoutesEnum.TIMER,
    getComponent(location, cb) {
        require.ensure([], (require) => {
            cb(null, require('./timer.component'))
        }, 'timer');
    }
};

if (module.hot) {
    module.hot.accept();
}