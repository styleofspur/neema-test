import { connect } from 'react-redux';

import { TimerActions }        from '../../actions'
import { Time, ButtonsHolder } from '../../components';

class Timer extends React.Component {

    _timer      = null;
    _maxSeconds = 3599;

    static elementClass = 'timer';

    constructor() {
        super(...arguments);

        this.startTimer = this.startTimer.bind(this);
        this.pauseTimer = this.pauseTimer.bind(this);
        this.clearTimer = this.clearTimer.bind(this);
    }

    startTimer() {
        this.props.dispatch(TimerActions.start());

        this._timer = setInterval(() => {
            this.props.dispatch(
                this.props.seconds < this._maxSeconds ? TimerActions.increaseTime() : TimerActions.clear()
            );
        }, 1000);
    }

    pauseTimer() {
        clearInterval(this._timer);
        this._timer = null;
        this.props.dispatch(TimerActions.pause());
    }

    clearTimer() {
        if (this._timer) {
            clearInterval(this._timer);
            this._timer = null;
        }

        this.props.dispatch(TimerActions.clear());
    }

    componentWillUnmount() {
        this.clearTimer();
    }

    render() {
        return (
            <div className={Timer.elementClass}>
                <h1 className="text-center">Timer App</h1>
                <div className="time-holder center-block">
                    <Time cssClass="center-block" seconds={this.props.seconds}/>
                    <ButtonsHolder
                        isRunning={this.props.isRunning}
                        onStart={this.startTimer}
                        onPause={this.pauseTimer}
                        onClear={this.clearTimer}
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({timer}) => {
    return {...timer};
};

export default connect(mapStateToProps)(Timer);