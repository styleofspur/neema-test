import { Header } from '../../containers';
import './app.styl';

export default class App extends React.Component {

    render() {
        return (
            <div className="app-container">
                <Header/>
                <div className="container page-container">
                    { this.props.children }
                </div>
            </div>
        );
    }
};

