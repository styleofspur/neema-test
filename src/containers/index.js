import AppComponent    from './app/app.component';
import HeaderComponent from './header/header.component';

export const App    = AppComponent;
export const Header = HeaderComponent;

if (module.hot) {
    module.hot.accept();
}