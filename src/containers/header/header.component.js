import { connect }         from 'react-redux';
import { IndexLink, Link } from 'react-router';

import './header.styl';

class Header extends React.Component {

    static elementClass = 'header';

    constructor() {
        super(...arguments);

        this.getLinkActiveClass = this.getLinkActiveClass.bind(this);
    }

    /**
     *
     * @param {string|Array<string>} location
     * @return {string}
     */
    getLinkActiveClass(location = '/') {
        if (Array.isArray(location)) {
            return location.includes(this.props.currentState) ? 'active' : '';
        } else {
            return this.props.currentState === location ? 'active' : '';
        }
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        return (
            <header className={Header.elementClass}>
                <nav className="navbar navbar-inverse">
                    <div className="container">
                        <div className="navbar-header">
                            <IndexLink to="/" className="navbar-brand">React Timer App</IndexLink>
                        </div>
                        <div className="navbar-collapse">
                            <ul className="nav navbar-nav">
                                <li className={this.getLinkActiveClass(['/', '/timer'])}>
                                    <Link to="/timer">Timer</Link>
                                </li>
                                <li className={this.getLinkActiveClass('/countdown')}>
                                    <Link to="/countdown">Countdown</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
        )
    }
}

/**
 *
 * @param routing
 * @returns {{currentState}}
 */
const mapStateToProps = ({routing}) => {
    return {
        currentState: routing.locationBeforeTransitions.pathname
    }
};

export default connect(mapStateToProps)(Header);