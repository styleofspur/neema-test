import ReactDOM                          from 'react-dom';
import { Router, match, browserHistory } from 'react-router';
import { syncHistoryWithStore }          from 'react-router-redux';
import { Provider }                      from 'react-redux';
import createStore                       from './store';

import routes          from './routes';
import { StoreHelper } from './helpers';
import './style.styl';

const location = window.location.pathname;
const rootEl   = document.getElementById('app');

const renderApp = (store, history, key) => {
    const routes = require('./routes');
    ReactDOM.render(
        <Provider store={store}>
            <Router routes={routes} history={history} key={key} />
        </Provider>,
        rootEl
    );
};

const hmr = () => {
    if (module.hot) {
        module.hot.accept(['./app', './routes'], () => {
            renderApp(store, history, Math.random());
        });
    }
};

const store   = createStore();
const history = syncHistoryWithStore(browserHistory, store);

Promise.resolve().then(() => {
    return StoreHelper.init(store);
}).then(() => {
    match({routes, location}, () => renderApp(store, history, Math.random()));
    hmr();
}).catch(console.error);