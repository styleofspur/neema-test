import { ActionsEnum } from '../utils/enums';

export const start = () => {
    return {
        type: ActionsEnum.TIMER.START
    };
};

export const increaseTime = () => {
    return {
        type: ActionsEnum.TIMER.INCREASE_TIME
    }
};

export const pause = () => {
    return {
        type: ActionsEnum.TIMER.PAUSE
    };
};

export const clear = () => {
    return {
        type: ActionsEnum.TIMER.CLEAR
    };
};