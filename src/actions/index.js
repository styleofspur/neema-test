import * as Timer     from './timer';
import * as Countdown from './countdown';

export const TimerActions     = Timer;
export const CountdownActions = Countdown;

if (module.hot) {
    module.hot.accept();
}