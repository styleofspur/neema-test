import { ActionsEnum } from '../utils/enums';

export const start = seconds => {
    return {
        type: ActionsEnum.COUNTDOWN.START,
        seconds
    };
};

export const decreaseTime = () => {
    return {
        type: ActionsEnum.COUNTDOWN.DECREASE_TIME
    }
};

export const pause = () => {
    return {
        type: ActionsEnum.COUNTDOWN.PAUSE
    };
};

export const clear = () => {
    return {
        type: ActionsEnum.COUNTDOWN.CLEAR
    };
};