# Table of Contents

* [Intro](#markdown-header-intro)
* [Technologies stack](#markdown-header-technologies-stack)
* [Prerequisites](#markdown-header-prerequisites)
* [Start](#markdown-header-start)

## Intro
This is the solution for Neema test task.
  
## Technologies stack
* building: [webpack](http://webpack.github.io) with [hot module replacement](http://webpack.github.io/docs/hot-module-replacement.html), and [babel](http://babeljs.io);
* core: [React](https://facebook.github.io/react) and [Redux](https://github.com/reactjs/redux)
* styling with [bootstrap-stylus](https://github.com/maxmx/bootstrap-stylus)

## Prerequisites
* Node.js v6.* 

## Start
Simply run `npm install` for dependencies installation and then `npm start` in order to run application.
The app will be available on [http://localhost:8000](http://localhost:8000).